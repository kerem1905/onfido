//
//  ViewController.swift
//  OnfidoTaskDemoApp
//
//  Created by Kerem Gunduz on 26.08.2018.
//  Copyright © 2018 Kerem Gunduz. All rights reserved.
//

import UIKit
import OnfidoTask

class ViewController: UIViewController {

    private lazy var coordinator: SelfieCoordinator = SelfieCoordinator()

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        coordinator.delegate = self
        let button: UIButton = UIButton(type: .roundedRect)
        button.accessibilityIdentifier = "StartButton"
        button.setTitle("Start !", for: .normal)
        button.addTarget(self, action: #selector(ViewController.buttonTapped), for: .touchUpInside)
        button.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(button)
        view.centerXAnchor.constraint(equalTo: button.centerXAnchor).isActive = true
        view.centerYAnchor.constraint(equalTo: button.centerYAnchor).isActive = true
        button.heightAnchor.constraint(equalToConstant: 50).isActive = true
        button.widthAnchor.constraint(equalToConstant: 150).isActive = true
    }

    @objc func buttonTapped() {
        coordinator.start(controller: self, autoSavePhotoToLibrary: true)
    }
}

extension ViewController: SelfieCoordinatorDelegate {
    func selfieProcessCompleted(with result: SelfieResult) {
        switch result {
        case .success(let image):
            let imageController = ImagePreviewController(image: image)
            self.navigationController?.pushViewController(imageController, animated: true)
        default: break
        }
    }
}
