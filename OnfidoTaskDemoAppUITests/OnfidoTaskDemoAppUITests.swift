//
//  OnfidoTaskDemoAppUITests.swift
//  OnfidoTaskDemoAppUITests
//
//  Created by Kerem Gunduz on 28.08.2018.
//  Copyright © 2018 Kerem Gunduz. All rights reserved.
//

import XCTest

class OnfidoTaskDemoAppUITests: XCTestCase {

    private lazy var app: XCUIApplication = XCUIApplication()

    override func setUp() {
        super.setUp()
        continueAfterFailure = false
        app.launch()
    }

    override func tearDown() {
        super.tearDown()
    }

    func testExample() {
        self.addUIInterruptionMonitor(withDescription: "Authorization Prompt") {
            $0.buttons["Allow"].tap()
            return true
        }
        app.buttons["StartButton"].tap()
    }
}
