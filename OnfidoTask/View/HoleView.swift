//
//  HoleView.swift
//  OnfidoTask
//
//  Created by Kerem Gunduz on 27.08.2018.
//  Copyright © 2018 Kerem Gunduz. All rights reserved.
//

import UIKit

class HoleView: UIView {

    private var roundedRectFrame: CGRect!

    // MARK: - Check if frame is inside of hole
    func insideHole(frame: CGRect) -> Bool {
        return roundedRectFrame.contains(frame)
    }

    // MARK: - Drawing
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        let holeWidth = rect.width * 0.7
        let holeHeight = rect.height * 0.5
        roundedRectFrame = CGRect(x: (rect.width-holeWidth)/2, y: (rect.height-holeHeight)/2, width: holeWidth, height: holeHeight)
        UIColor(red: 0, green: 0, blue: 0, alpha: 0.7).setFill()
        UIRectFill(rect)

        let layer = CAShapeLayer()
        let path = CGMutablePath()

        path.addRoundedRect(in: roundedRectFrame, cornerWidth: holeWidth/3,
                  cornerHeight: holeWidth/3, transform: .identity)
        path.addRect(bounds)
        layer.path = path
        layer.fillRule = kCAFillRuleEvenOdd
        self.layer.mask = layer
    }

    // MARK: - Initialization
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
    }
}
