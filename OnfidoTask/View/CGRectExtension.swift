//
//  CGRectExtension.swift
//  OnfidoTask
//
//  Created by Kerem Gunduz on 27.08.2018.
//  Copyright © 2018 Kerem Gunduz. All rights reserved.
//

import Foundation

extension CGRect {
    func scaled(to size: CGSize) -> CGRect {
        return CGRect(
            x: origin.x * size.width,
            y: origin.y * size.height,
            width: self.size.width * size.width,
            height: self.size.height * size.height
        )
    }
}
