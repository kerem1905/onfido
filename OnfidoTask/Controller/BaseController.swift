//
//  BaseController.swift
//  OnfidoTask
//
//  Created by Kerem Gunduz on 27.08.2018.
//  Copyright © 2018 Kerem Gunduz. All rights reserved.
//

import UIKit

class BaseController: UIViewController {

    /**
     If any subclass wants to hide navigation bar, then override
     this property and return true
     */
    var navigationBarTransparent: Bool {
        return false
    }

    /**
     Shorthand methods for subclasses to perform
     UI staff
     */
    func configureUI() {
        if navigationBarTransparent {
            navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
            navigationController?.navigationBar.isTranslucent = true
            navigationController?.navigationBar.shadowImage = UIImage()
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        configureUI()
    }
}
