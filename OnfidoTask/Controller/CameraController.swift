//
//  CameraController.swift
//  OnfidoTask
//
//  Created by Kerem Gunduz on 26.08.2018.
//  Copyright © 2018 Kerem Gunduz. All rights reserved.
//

import UIKit
import AVFoundation
import Vision
import Photos

protocol CameraControllerDelegate: AnyObject {
    func controllerProcessCompleted(with result: SelfieResult)
}

class CameraController: BaseController {

    // MARK: Delegation Objects
    weak var delegate: CameraControllerDelegate?

    private var autoSavePhoto: Bool = false

    // MARK: UIKit Objects
    private var overlayView: HoleView!
    private var labelWarning: UILabel!

    // MARK: AVFoundation Objects
    private var session: AVCaptureSession?
    private var videoPreviewLayer: AVCaptureVideoPreviewLayer?
    private var frontCamera: AVCaptureDevice? = {
        return AVCaptureDevice.default(AVCaptureDevice.DeviceType.builtInWideAngleCamera, for: AVMediaType.video, position: .front)
    }()

    // MARK: Vision Objects
    private let faceDetection = VNDetectFaceRectanglesRequest()
    private let faceDetectionRequest = VNSequenceRequestHandler()

    // MARK: Core Image
    private let context = CIContext()
    private var faceDetector: CIDetector?

    // MARK: GCD Queues
    private let detectionQueue = DispatchQueue(label: "com.onfido.queue.detection")
    private let dismissQueue = DispatchQueue(label: "com.onfido.queue.dismiss")
    private let bufferQueue = DispatchQueue(label: "com.onfido.queue.sampelbuffer")

    private var dismissInProgress: Bool = false

    override var navigationBarTransparent: Bool {
        return true
    }

    // MARK: Init Methods
    convenience init() {
        self.init(autoSavePhoto: false)
    }

    init(autoSavePhoto: Bool) {
        self.autoSavePhoto = autoSavePhoto
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    // MARK: View Lifecycle Methods
    override func viewDidLoad() {
        sessionPrepare()
        super.viewDidLoad()
    }

    /**
     Stop Capture Session when view will appear
     */
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        session?.stopRunning()
    }

    /**
     Start Capture Session when view will appear
     */
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        session?.startRunning()
    }

    /**
     Setting up UI elements
     */
    override func configureUI() {
        super.configureUI()
        videoPreviewLayer = AVCaptureVideoPreviewLayer(session: session!)
        videoPreviewLayer?.videoGravity = AVLayerVideoGravity.resizeAspectFill
        view.layer.addSublayer(videoPreviewLayer!)
        view.backgroundColor = .clear
        overlayView = HoleView()
        overlayView.backgroundColor = .clear
        overlayView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(overlayView)
        view.leftAnchor.constraint(equalTo: overlayView.leftAnchor).isActive = true
        view.rightAnchor.constraint(equalTo: overlayView.rightAnchor).isActive = true
        view.topAnchor.constraint(equalTo: overlayView.topAnchor).isActive = true
        view.bottomAnchor.constraint(equalTo: overlayView.bottomAnchor).isActive = true
        view.bringSubview(toFront: overlayView)
        labelWarning = UILabel()
        labelWarning.font = UIFont.systemFont(ofSize: 14)
        labelWarning.textColor = .white
        labelWarning.numberOfLines = 0
        labelWarning.textAlignment = .center
        labelWarning.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(labelWarning)
        labelWarning.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -40).isActive = true
        labelWarning.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 20).isActive = true
        view.rightAnchor.constraint(equalTo: labelWarning.rightAnchor, constant: 20).isActive = true
        view.bringSubview(toFront: labelWarning)
    }

    /**
     Set videoPreviewLayer frame when bounds change
     */
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        videoPreviewLayer?.frame = view.layer.bounds
    }

    /**
     Setting Warning Label text
     - parameters:
     - text: String to set UILabel
     */
    func setWarningText(_ text: String) {
        DispatchQueue.main.async {
            self.labelWarning.text = text
        }
    }
}

// MARK: CameraSession Initialize
extension CameraController {

    /**
     Initialize CameraSession object, put required inputs on it
     and initalize faceDetector.
     */
    func sessionPrepare() {
        faceDetector = CIDetector(ofType: CIDetectorTypeFace, context: nil, options: [CIDetectorAccuracy: CIDetectorAccuracyHigh])
        session = AVCaptureSession()
        guard let session = session, let captureDevice = frontCamera else { return }
        do {
            let deviceInput = try AVCaptureDeviceInput(device: captureDevice)
            session.beginConfiguration()
            if session.canAddInput(deviceInput) {
                session.addInput(deviceInput)
            }
            let output = AVCaptureVideoDataOutput()
            output.videoSettings = [
                String(kCVPixelBufferPixelFormatTypeKey): Int(kCVPixelFormatType_420YpCbCr8BiPlanarFullRange)
            ]
            output.alwaysDiscardsLateVideoFrames = true
            if session.canAddOutput(output) {
                session.addOutput(output)
            }
            session.commitConfiguration()
            output.setSampleBufferDelegate(self, queue: bufferQueue)
        } catch {
            delegate?.controllerProcessCompleted(with: .cameraSessionCouldNotSetup(error.localizedDescription))
        }
    }
}

// MARK: AVCaptureOutput Methods
extension CameraController: AVCaptureVideoDataOutputSampleBufferDelegate {

    /**
     After BufferQueue calls this method, converts pixel buffer
     to CIImage and then try to find if any single face exists
     - parameters:
     - image: Source Image to find face
     */
    func captureOutput(_ output: AVCaptureOutput, didOutput sampleBuffer: CMSampleBuffer, from connection: AVCaptureConnection) {
        let pixelBuffer = CMSampleBufferGetImageBuffer(sampleBuffer)
        let attachments = CMCopyDictionaryOfAttachments(kCFAllocatorDefault, sampleBuffer, kCMAttachmentMode_ShouldPropagate)
        let ciImage = CIImage(cvImageBuffer: pixelBuffer!, options: (attachments as? [String: Any]))
        let ciImageWithOrientation = ciImage.oriented(forExifOrientation: Int32(UIImageOrientation.leftMirrored.rawValue))
        detectFace(on: ciImageWithOrientation)
    }
}

// MARK: Detect Methods
extension CameraController {

    /**
     Detecting if any face exists on CIImage
     - parameters:
     - image: Source Image to find face
     */
    func detectFace(on image: CIImage) {
        try? faceDetectionRequest.perform([faceDetection], on: image)
        if let results = faceDetection.results as? [VNFaceObservation] {
            if results.count == 1 {
                self.detectFeatures(on: image)
                DispatchQueue.main.async {
                    let rect = results[0].boundingBox.scaled(to: self.view.bounds.size)
                    if self.overlayView.insideHole(frame: rect) {
                        self.detectionQueue.sync {
                            if let cgImage = self.context.createCGImage(image, from: image.extent) {
                                let image = UIImage(cgImage: cgImage, scale: 1, orientation: .upMirrored)
                                self.dismiss(with: image)
                            }
                        }
                    } else {
                        self.setWarningText("Place your face inside oval shape !")
                    }
                }
            }
        }
    }

    /**
     Detecting face features to notify user
     - parameters:
     - image: Source Image to use
     */
    func detectFeatures(on image: CIImage) {
        let features = self.faceDetector!.features(in: image, options: [CIDetectorSmile: true, CIDetectorEyeBlink: true])
        if features.count != 0 {
            for feature in features as! [CIFaceFeature] {
                if feature.leftEyeClosed || feature.rightEyeClosed {
                    self.setWarningText("Open your eyes 👀")
                } else if !feature.hasSmile {
                    self.setWarningText("Smile 😁")
                } else {
                    self.setWarningText("")
                }
            }
        }
    }

    /**
     Dismissing viewcontroller then callback via delegate object
     - parameters:
     - image: Captured image to pass delegate method
     */
    private func dismiss(with image: UIImage) {
        if !dismissInProgress {
            dismissQueue.sync {
                dismissInProgress = true
                if self.autoSavePhoto {
                    PhotoLibraryHelper.saveImageToLibrary(image: image, completion: { _ in
                        DispatchQueue.main.async {
                            self.dismiss(animated: true, completion: {
                                self.delegate?.controllerProcessCompleted(with: .success(image))
                            })
                        }
                    })
                } else {
                    DispatchQueue.main.async {
                        self.dismiss(animated: true, completion: {
                            self.delegate?.controllerProcessCompleted(with: .success(image))
                        })
                    }
                }
            }
        }
    }
}
