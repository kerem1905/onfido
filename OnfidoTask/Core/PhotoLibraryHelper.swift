//
//  PhotoLibraryHelper.swift
//  OnfidoTask
//
//  Created by Kerem Gunduz on 27.08.2018.
//  Copyright © 2018 Kerem Gunduz. All rights reserved.
//

import Photos

class PhotoLibraryHelper {

    /**
     Saves Images to Photo Library
     - parameters:
     - image: target image to save
     - completion: callback block
     */
    static func saveImageToLibrary(image: UIImage, completion: @escaping (Bool) -> Void) {
        PHPhotoLibrary.requestAuthorization { status in
            guard status == .authorized else {
                print("Image could not saved to photo roll. Error: User photo library access is not granted")
                completion(false)
                return
            }
            PHPhotoLibrary.shared().performChanges({
                PHAssetChangeRequest.creationRequestForAsset(from: image)
            }, completionHandler: { success, error in
                if !success {
                    print("Image could not saved to photo roll. Error: \(error.debugDescription)")
                }
                completion(success)
            })
        }

    }
}
