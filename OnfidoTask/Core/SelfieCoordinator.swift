//
//  CameraCoordinator.swift
//  OnfidoTask
//
//  Created by Kerem Gunduz on 26.08.2018.
//  Copyright © 2018 Kerem Gunduz. All rights reserved.
//

public protocol SelfieCoordinatorDelegate: AnyObject {
    func selfieProcessCompleted(with result: SelfieResult)
}

enum State {
    case started
    case ended
    case none
}
open class SelfieCoordinator {

    public weak var delegate: SelfieCoordinatorDelegate?
    private var state: State = .none // default state is none

    public init() { } // public default constructor

    // MARK: - Start Flow
    open func start(controller: UIViewController?, autoSavePhotoToLibrary: Bool = false) {
        guard state != .started else {
            return
        }
        state = .started
        CameraAuthorizationHelper.checkAuthorization { [unowned self] (status) in
            guard status == .authorized else {
                self.delegate?.selfieProcessCompleted(with: .cameraAccessNotGranted("Camera access is not granted"))
                return
            }
            DispatchQueue.main.async {
                let camController = CameraController(autoSavePhoto: autoSavePhotoToLibrary)
                camController.delegate = self
                let navcontroller = UINavigationController(rootViewController: camController)
                if let controller = controller {
                    controller.present(navcontroller, animated: true, completion: nil)
                } else if let window = UIApplication.shared.keyWindow {
                    window.rootViewController?.present(navcontroller, animated: true, completion: nil)
                }

            }

        }
    }
}

// MARK: - Controller Callback Delegate
extension SelfieCoordinator: CameraControllerDelegate {
    func controllerProcessCompleted(with result: SelfieResult) {
        state = .ended
        delegate?.selfieProcessCompleted(with: result)
    }
}
