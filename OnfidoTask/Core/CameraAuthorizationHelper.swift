//
//  AuthorizationHelper.swift
//  OnfidoTask
//
//  Created by Kerem Gunduz on 26.08.2018.
//  Copyright © 2018 Kerem Gunduz. All rights reserved.
//

import AVFoundation

class CameraAuthorizationHelper {

    /**
     Check user authorization if accesses are granded
     - parameters:
     - completion: callback block
     */
    static func checkAuthorization(completion: @escaping (AVAuthorizationStatus) -> Void) {
        let status = AVCaptureDevice.authorizationStatus(for: AVMediaType.video)
        switch status {
        case .notDetermined:
            AVCaptureDevice.requestAccess(for: AVMediaType.video, completionHandler: { granted in
                if granted {
                    completion(.authorized)
                } else {
                    completion(.denied)
                }
            })
        default:
            completion(status)
        }
    }
}
