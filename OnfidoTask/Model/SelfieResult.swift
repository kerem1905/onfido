//
//  SelfieResult.swift
//  OnfidoTask
//
//  Created by Kerem Gunduz on 27.08.2018.
//  Copyright © 2018 Kerem Gunduz. All rights reserved.
//

public enum SelfieResult {
    case cameraAccessNotGranted(String)
    case cameraSessionCouldNotSetup(String)
    case success(UIImage)
}
