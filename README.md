
# OnfidoSDK-iOS

## Overview

Basically OnfidoSDK lets you take selfie automatically when detects a single face in camera.

## Getting Started

This SDK is compatible with IOS 11.0 and Swift 4.0 above

### Permissions

  
This SDK requires to use of the device Camera and Photo Library. You will be required to have the  `NSCameraUsageDescription`  and  `NSPhotoLibraryUsageDescription`  keys in your application's  `Info.plist`  file:

    <key>NSCameraUsageDescription</key>
    <string>Demo Camera Usage Description</string>
    <key>NSPhotoLibraryUsageDescription</key>
    <string>Demo Photo Library Usage Description</string>

### Adding the SDK dependency

You can either add SDK with cocoapods or directly adding framework file to your project.
If you want to add SDK with cocoapods, you should add the following to your Podfile:

    pod 'OnfidoTask'

Run  `pod install`  to install the SDK.

### Configuration & Starting Flow

*Example Usage:*

```swift
class ViewController: UIViewController {

    private lazy var coordinator: SelfieCoordinator = SelfieCoordinator()

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        coordinator.delegate = self
        let button: UIButton = UIButton(type: .roundedRect)
        button.setTitle("Start !", for: .normal)
        button.addTarget(self, action: #selector(ViewController.buttonTapped), for: .touchUpInside)
        button.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(button)
        view.centerXAnchor.constraint(equalTo: button.centerXAnchor).isActive = true
        view.centerYAnchor.constraint(equalTo: button.centerYAnchor).isActive = true
        button.heightAnchor.constraint(equalToConstant: 50).isActive = true
        button.widthAnchor.constraint(equalToConstant: 150).isActive = true
    }

    @objc func buttonTapped() {
        coordinator.start(controller: self, autoSavePhotoToLibrary: true)
    }
}

    ...
```
 Since SelfieCoordinator object stores states on itself, it would be better to instantiate it as singleton then set delegate to your own class.
 
 #### Start Parameters

 - controller: If this parameter is not nil, then passed viewcontroller will be used to present SDK s viewcontollers.
 - autoSavePhotoToLibrary: Determines if captured image should saved to photo library directly or not. Default value is false.

## Handling Callbacks

The callback method can be used for both handling errors and success cases

*Example Usage:*
```swift

    

    public enum SelfieResult {
            case cameraAccessNotGranted(String)
            case cameraSessionCouldNotSetup(String)
            case success(UIImage)
        }
    
    
        extension ViewController: SelfieCoordinatorDelegate {
            func selfieProcessCompleted(with result: SelfieResult) {
                switch result {
                case .success(let image):
                    let imageController = ImagePreviewController(image: image)
                    self.navigationController?.pushViewController(imageController, animated: true)
                default: break
                }
            }
        }

```



