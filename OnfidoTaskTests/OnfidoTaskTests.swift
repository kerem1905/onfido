//
//  OnfidoTaskTests.swift
//  OnfidoTaskTests
//
//  Created by Kerem Gunduz on 26.08.2018.
//  Copyright © 2018 Kerem Gunduz. All rights reserved.
//

import XCTest
@testable import OnfidoTask

class OnfidoTaskTests: XCTestCase {
    var app: XCUIApplication!

    // MARK: - XCTestCase

    override func setUp() {
        super.setUp()

        // Since UI tests are more expensive to run, it's usually a good idea
        // to exit if a failure was encountered
        continueAfterFailure = false

        app = XCUIApplication()

        // We send a command line argument to our app,
        // to enable it to reset its state
        app.launchArguments.append("--uitesting")
    }

    // MARK: - Tests

    func testGoingThroughOnboarding() {
        app.launch()

    
        // Swipe left three times to go through the pages
        app.swipeLeft()
        app.swipeLeft()
        app.swipeLeft()

        // Tap the "Done" button
        app.buttons["Done"].tap()

        // Onboarding should no longer be displayed
        XCTAssertFalse(app.isDisplayingOnboarding)
    }
}
